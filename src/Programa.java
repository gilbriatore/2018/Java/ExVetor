
public class Programa {

	public static void main(String[] args) 
	{
	
		char[] vetor = new char[7];
		vetor[0] = 'P';
		vetor[1] = 'A';
		vetor[2] = 'L';
		vetor[3] = 'A';
		vetor[4] = 'V';
		vetor[5] = 'R';
		vetor[6] = 'A';
		
		//5a forma de impress�o inversa
		int indice = 6;
		do {
			System.out.print(vetor[indice]);
			indice--;
		} while(indice >=0);
		
		
		/**
		//4a forma de impress�o inversa
		int indice = 6;
		while(indice >= 0)
		{
			System.out.print(vetor[indice]);
			indice--;
		}
		*/
		
		
		/**
		//3a forma de impress�o inversa
		//for(int indice = 0; indice < 7; indice++) //crescente
		for(int indice = 6; indice >= 0; indice--)//decrescente
		{
			System.out.print(vetor[indice]);
		}
		*/
		
		/**
		//2a forma de impress�o inversa
		System.out.print(vetor[6]);
		System.out.print(vetor[5]);
		System.out.print(vetor[4]);
		System.out.print(vetor[3]);
		System.out.print(vetor[2]);
		System.out.print(vetor[1]);
		System.out.print(vetor[0]);
		*/
		
		/**
		//1a forma de impress�o inversa
		System.out.println(
				vetor[6] +""+ vetor[5] +""+ 
		        vetor[4] +""+ vetor[3] +""+ 
				vetor[2] +""+ vetor[1] +""+ vetor[0]);
				*/
		
	}

}
